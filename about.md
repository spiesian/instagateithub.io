---
layout: page
title: INSTAGATE | About Us
description: >-
    When building a website it's helpful to see what the focus of your site is.
    This page is an example of how to show a website's focus.
sitemap:
    priority: 0.7
    lastmod: 2017-11-02 00:00:00
    changefreq: weekly
---

## About Us

<span class="image left">&lt;img src="{{ "/images/download.png" | absolute_url }}" alt="" /&gt;</span>

**We aim to Instigate innovation, strategy, and preemptive security.**

Research in following all available data from multiple silos to assure that your Intellectual Property is recovered or properly protected. We want to make sure that the everyday working man knows their rights on intellectual Property while working for their employer or company during a project in which the development of a patented art is submitted to the patent office. We bring extensive experience and professionalism to every case and customize our support to your individual needs and concerns.

Instagate Technology, is a company that would effectively intersect our expertise with our respective interests. We believe that what makes a creative business thrive is the passion that drives it. Choosing to specialize in a field means that you are completely devoted to it; that you will protect it as if it were your own. That is the energy that sets us apart.
