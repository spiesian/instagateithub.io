---
layout: page
title: INSTAGATE | Contact
description: This page is an example of how to direct your viewers to your avaialble methods of communication.
sitemap:
    priority: 0.7
    lastmod: 2017-11-02
    changefreq: weekly
---

<h2>Contact Us</h2>



<div class="container text-center">
  <div class="columns">
    <div class="column col-6 col-mx-auto col-sm-12 negative-m-xs">
      <div class="card">
        <div class="card-body">
                <b>PLEASE READ FIRST!</b>
<p>To help us best serve your inquiry, we recommend that you first describe the issue you’re having before telling us what you want to achieve.  Due to privacy and the core values of our company we urge all members and visitors to keep in mind to use discretion for all matters concerning Intellectual Property and utilize our security instruments and channels.  You may also email or call us to make an appointment and we will do our best to respond promptly.  Our general response time is one business day.</p>
        </div>
      </div>
    </div>
  </div>
</div>

